package sample.app.petsearchassignment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import java.io.UnsupportedEncodingException;
import sample.app.petsearchassignment.model.Genre;
import sample.app.petsearchassignment.model.MovieDetails;
import sample.app.petsearchassignment.utility.ApplicationUrl;

public class MovieDetailsActivity extends AppCompatActivity {

    private ImageView image;
    private TextView movieOverview, movieDuration, movieDate, movieLanguage, movieGener, movieBudget, movieRevenue, movieRating, movieTitle;
    private int movieId;
    private static String apiKey ="?api_key=9f4ec3a600085be0c65613e106d55b3d&language=en-US";
    private static String imagePath = "https://image.tmdb.org/t/p/w500";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movie_details);

        init();
    }

    //initialize variables
    private void init(){

        Intent intent = this.getIntent();
        if(intent !=null)// to avoid the NullPointerException
            movieId = intent.getExtras().getInt("movieId");

        image = findViewById(R.id.image);
        movieTitle = findViewById(R.id.movieTitle);
        movieOverview = findViewById(R.id.movieOverview);
        movieDuration = findViewById(R.id.duration);
        movieDate = findViewById(R.id.releaseDate);
        movieLanguage = findViewById(R.id.language);
        movieGener = findViewById(R.id.gener);
        movieBudget = findViewById(R.id.budgetValue);
        movieRevenue = findViewById(R.id.revenueValue);
        movieRating = findViewById(R.id.movieRating);

        showMovieDetails();

    }

    //fetch data from server using volley and gson, show data of the item
    private void showMovieDetails(){

        StringRequest stringRequest = new StringRequest(Request.Method.GET, ApplicationUrl.MOVIE_DETAIL_URL+movieId+apiKey,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Gson gson = new GsonBuilder().create();
                        MovieDetails movieDetails = gson.fromJson(response, MovieDetails.class);

                        Picasso.get().load(imagePath+movieDetails.getBackdropPath()).into(image);
                        movieTitle.setText(movieDetails.getTitle());
                        movieOverview.setText(movieDetails.getOverview());
                        movieDate.setText(movieDetails.getReleaseDate());
                        movieLanguage.setText(movieDetails.getOriginalLanguage());
                        movieBudget.setText("$"+movieDetails.getBudget().toString());
                        movieRevenue.setText("$"+movieDetails.getRevenue().toString());
                        movieDuration.setText(movieDetails.getRuntime().toString()+"minutes");
                        movieRating.setText(movieDetails.getVoteAverage().toString());

                        for(Genre genre : movieDetails.getGenres()){
                            movieGener.append(", "+ genre.getName());
                        }
                        movieGener.setText(movieGener.getText().toString().replaceFirst(", ", ""));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        NetworkResponse response = error.networkResponse;
                        Log.d("Error", "" + error);
                        if (response == null) {
                        }
                    }
                })
        {
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                try {
                    if (response.data.length == 0) {
                        byte[] responseData = "{}".getBytes("UTF8");
                        response = new NetworkResponse(response.statusCode, responseData, response.headers, response.notModified);
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.parseNetworkResponse(response);
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}