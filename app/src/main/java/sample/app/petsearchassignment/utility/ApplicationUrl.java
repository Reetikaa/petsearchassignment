package sample.app.petsearchassignment.utility;

public class ApplicationUrl {

    public final static String MOVIE_URL ="https://api.themoviedb.org/3/discover/movie?api_key=9f4ec3a600085be0c65613e106d55b3d&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1";

    public final static String MOVIE_DETAIL_URL = "https://api.themoviedb.org/3/movie/";

}
