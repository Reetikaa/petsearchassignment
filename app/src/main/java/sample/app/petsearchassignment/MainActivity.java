package sample.app.petsearchassignment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import sample.app.petsearchassignment.Interface.RecyclerViewItemClick;
import sample.app.petsearchassignment.adapter.MovieAdapter;
import sample.app.petsearchassignment.model.Movie;
import sample.app.petsearchassignment.model.Result;
import sample.app.petsearchassignment.utility.ApplicationUrl;

public class MainActivity extends AppCompatActivity {

    private static String TAG = MainActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private List<Result> movieList;
    private MovieAdapter movieAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    //initialize variables
    private void init(){
        recyclerView = findViewById(R.id.recyclerView);

        movieList = new ArrayList<Result>();
        movieAdapter = new MovieAdapter(this, new RecyclerViewItemClick() {
            @Override
            public void onItemClick(int position, Object data) {
                // TODO handle click
                Log.d(TAG, "Item clicked at "+position);

            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(movieAdapter);

        getMovieDetails();
    }

    //fetch data from server using volley and gson
    private void getMovieDetails(){

        StringRequest stringRequest = new StringRequest(Request.Method.GET, ApplicationUrl.MOVIE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Gson gson = new GsonBuilder().create();
                        Movie movie = gson.fromJson(response, Movie.class);

                        for(Result result : movie.getResults()){
                            movieList.add(result);
                        }

                        movieAdapter.updateList(movieList);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        NetworkResponse response = error.networkResponse;
                        Log.d("Error", "" + error);
                        if (response == null) {

                            String errorMessage = error.getClass().getSimpleName();

                        }
                    }
                })

        {

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                try {
                    if (response.data.length == 0) {
                        byte[] responseData = "{}".getBytes("UTF8");
                        response = new NetworkResponse(response.statusCode, responseData, response.headers, response.notModified);
                        Log.d("", "NetworkResponce"+response+"");
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.parseNetworkResponse(response);
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
