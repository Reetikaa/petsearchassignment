package sample.app.petsearchassignment.Interface;

public interface RecyclerViewItemClick {

    public void onItemClick(int position, Object data);
}
