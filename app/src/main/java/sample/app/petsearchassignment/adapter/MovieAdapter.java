package sample.app.petsearchassignment.adapter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;
import sample.app.petsearchassignment.Interface.RecyclerViewItemClick;
import sample.app.petsearchassignment.MainActivity;
import sample.app.petsearchassignment.MovieDetailsActivity;
import sample.app.petsearchassignment.R;
import sample.app.petsearchassignment.model.Result;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MyViewHolder>{

    List<Result> movieList= new ArrayList<>(0);
    RecyclerViewItemClick recyclerViewItemClick;
    MainActivity activity;
    private static String imagePath = "https://image.tmdb.org/t/p/w500";

    public MovieAdapter(MainActivity activity, RecyclerViewItemClick recyclerViewItemClick) {
        this.recyclerViewItemClick = recyclerViewItemClick;
        this.activity = activity;
    }

    public void updateList(List<Result> newList){
        movieList.clear();
        movieList.addAll(newList);
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView movieName, movieRatings, movieOverview, movieLanguage, movieReleaseDate;
        private ImageView movieImage;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            movieName = itemView.findViewById(R.id.movieName);
            movieRatings = itemView.findViewById(R.id.movieRating);
            movieOverview = itemView.findViewById(R.id.movieOverview);
            movieLanguage = itemView.findViewById(R.id.language);
            movieReleaseDate = itemView.findViewById(R.id.releaseDate);
            movieImage = itemView.findViewById(R.id.image);
        }
    }

    @NonNull
    @Override
    public MovieAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.movie_list_item, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieAdapter.MyViewHolder holder, final int position) {

        final Result movie = movieList.get(position);
        holder.movieName.setText(movie.getTitle());
        holder.movieRatings.setText(movie.getVoteAverage().toString());
        holder.movieOverview.setText( movie.getOverview());
        holder.movieLanguage.setText(movie.getOriginalLanguage());
        holder.movieReleaseDate.setText(movie.getReleaseDate());

        String restaurantImage = imagePath+movie.getPosterPath();
        if( restaurantImage != null && !restaurantImage.isEmpty()){
            Picasso.get().load(restaurantImage).into(holder.movieImage);
        }
        else{
            Picasso.get().load(R.drawable.ic_launcher_background).into(holder.movieImage);
        }

        holder.movieImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerViewItemClick.onItemClick(position , activity);
                Intent intent = new Intent(activity, MovieDetailsActivity.class);
                intent.putExtra("movieId", movie.getId());
                activity.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }
}
